El desarrollo de las tecnologías de la información ha permeado las formas de trabajo, también en el ámbito académico. Los investigadores/as emplean diariamente una multitud de herramientas y servicios en línea, sin realizar siempre un análisis crítico de las consecuencias de su uso. En este proyecto planteamos la publicación de un manual divulgativo para que los académicos y académicas puedan acceder a él y cambiar sus hábitos en favor de las tecnologías libres.
Este proyecto se vincula directamente a la convocatoria planteada por “Girls 4 privacy” en dos sentidos. Primeramente, la utilización de herramientas libres contribuye al respeto de la privacidad de los usuarios/as y de los datos gestionados en ellas. Y, en segundo lugar, la comunidad científica, especialmente en Ciencias Sociales, se encuentra en continuo contacto con las sociedades que investiga. Por ello, es una necesidad de primer interés conocer cómo proteger no solo la información de las personas y comunidades que forman parte del estudio, sino también de las comunicaciones que se tienen con ellas, así como con el resto de la comunidad universitaria (profesorado y alumnado).
Pero, además, la utilización de software libre facilita la apertura del saber académico, el libre acceso a los métodos de investigación por parte de la ciudadanía. Y adicionalmente, las universidades, con un gran peso institucional, político y económico, resultan un apoyo relevante para el desarrollo y fortalecimiento de los proyectos de software libre existentes y el nacimiento de otros nuevos adaptados a necesidades concretas

# [Investigación social libre](https://gitlab.com/enacoding/socialsciencefreesoftware/-/blob/master/Manual/AlternativasInvestigacionSocialLibre.md)

## Herramientas libres para las Ciencias sociales 

   * **0. INTRODUCCIÓN**
   * **1. EXTRACCIÓN Y ANÁLISIS DE DATOS**
       * Software de análisis cuantitativo
       * Software de análisis cualitativo
       * Gráficos  y diagramas
       * Encuestas 
       * Mapas
       * Transcriptor de entrevistas y audio
   * **2.COMUNICACIÓN Y TRABAJO EN EQUIPO**
       * Transferencia de ficheros
       * Cliente de correo electrónico
       * Videoconferencia y chat
       * Agenda y trabajo en equipo
       * Cloud y bases de datos
       * Software de compresión
   * **3. OFIMÁTICA Y PRESENTACIÓN DE CONTENIDO**
       * Procesadores de texto
       * Documentación Portable PDF
       * Presentaciones
       * Editor de imágenes y vídeo
       * Maquetación
       * Traductor
   * **4. UTILIDADES Y OTRAS TAREAS DE INVESTIGACIÓN**
       * Gestor bibliográfico
       * Editor de CV
       * Navegador
       * Antivirus
       * Cifrado y protección de datos (PGP)

   * **5. Transición de software propietario a software libre (Interferencias)**

   * **6. OFICINAS DE SOFTWARE LIBRE EN LA UNIVERSIDAD**


