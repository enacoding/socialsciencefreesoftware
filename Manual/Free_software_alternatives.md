#  Free software alternatives / Alternativas de Software Libre

A continuación se muestra un listado de programas que responden a las necesidades de investigación en Ciencias Sociales. Este listado está sujeto a todo tipo de cambios, como los que puedas introducir tú si te apetece colaborar ;)


### Word processor / Procesador de texto

- Crytpad
- LibreOffice Write

### Portable PDF Documentation / Documentación Portable PDF
- Evince
- PDF.js

### Quantitative analysis software / Software de análisis cuantitativo
- PSPP
- R (programming language)
- Python

### Qualitative analysis software / Software de análisis cualitativo
- AQUA
- Taguette
- CATMA

### Graphs and diagrams / Gráficos  y diagramas
- GNUplot
- R and Python Libraries
- GeoGebra
- FreeMind
- LibreOffice - Draw

### Cloud and databases / Cloud y bases de datos
- Nextcloud
- OwnCloud

### File transfer / Transferencia de ficheros
- Syncthing
- Firefox Send
- pCloud
- Freenet 

### Presentations / Presentaciones
- LibreOffice - Impress
- Sozi

### Surveys / Encuestas
- LimeSurvey

### Image and video editor / Editor de imágenes y vídeo
- GIMP
- VCL

### Layout / Maquetación
- Scribus

### Maps / Mapas
- Ushahidi
- ArcGIS 

### Email client / Cliente de correo electrónico
- Tunderbird
- Disroot
- Protonmail
- Tutanota

### Interview and audio transcriber / Transcriptor de entrevistas y audio
- Audacity
- Wryte
- oTranscribe 
- Parlatype

### Translator / Traductor
- Apertium

### Bibliography / Bibliografía
- LinGen
- Repositories (DSpace, Invenio, Dataverse, Greenstone, MyCoRe)
- OpenDOAR
- Zenodo

### Reference manager / Gestor bibliográfico   
- Zotero
- Polar

### CV Editor / Editor de CV
- Europass

### Videoconference and chat / Videoconferencia y chat
- Jitsi
- Discord
- Mumble

### Agenda and teamwork / Agenda y trabajo en equipo
- Framadate - Framacalendar
- Hypothes.is
- GITLab
- Taiga.io
- Kanboard
- Trello

### Browser / Navegador
- TOR
- Firefox
- DuckDuckGo

### Antivirus / Antivirus
- Clam AntiVirus 

### Compression software / Software de compresión
- 7-ZIP
- File Roller 

### Social networking sites / Redes sociales
- Humanities Commons
- Is.net

### Others / Otros
- BleachBit (Space cleaner / limpiador de espacio en disco duro)
- Veracrypt (encriptación de memorias externas)
