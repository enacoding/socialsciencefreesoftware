# Investigación social libre

## Herramientas libres para las Ciencias sociales 



## Índice

   * **0. INTRODUCCIÓN**
   * **1. EXTRACCIÓN Y ANÁLISIS DE DATOS**
       * Software de análisis cuantitativo
       * Software de análisis cualitativo
       * Gráficos  y diagramas
       * Encuestas 
       * Mapas
       * Transcriptor de entrevistas y audio
   * **2.COMUNICACIÓN Y TRABAJO EN EQUIPO**
       * Transferencia de ficheros
       * Cliente de correo electrónico
       * Videoconferencia y chat
       * Agenda y trabajo en equipo
       * Cloud y bases de datos
       * Software de compresión
   * **3. OFIMÁTICA Y PRESENTACIÓN DE CONTENIDO**
       * Procesadores de texto
       * Documentación Portable PDF
       * Presentaciones
       * Editor de imágenes y vídeo
       * Maquetación
       * Traductor
   * **4. UTILIDADES Y OTRAS TAREAS DE INVESTIGACIÓN**
       * Gestor bibliográfico
       * Editor de CV
       * Navegador
       * Antivirus
       * Cifrado y protección de datos (PGP)


   * **5. Transición de software propietario a software libre (Interferencias)**
  [http://www.somoslibres.org/modules.php?name=News\&file=article\&sid=1374](http://www.somoslibres.org/modules.php?name=News\&file=article\&sid=1374)



   * **6. OFICINAS DE SOFTWARE LIBRE EN LA UNIVERSIDAD**




## **0. INTRODUCCIÓN**

Imaginad una facultad o centro de investigación con las ventanas negras. En su interior los estrechos pasillos se encuentran en silencio y las puertas permanecen cerradas: el conocimiento no fluye, no se conecta, se crea en salas herméticas a las que solo se puede entrar bajo la condición de pagar un precio por la entrada. En ese centro o facultad el conocimiento fluiría con dificultad y se parecía mucho al software privativo, que no permite conocer el código y en la mayoría de las ocasiones (con permiso del pirateo), es necesario comprar una licencia para poder emplearlo. Las alternativas que se presentan aquí cumplen la mayoría con las necesidades que tiene el personal investigador de Ciencias sociales. Estas alternativas han sido diseñadas precisamente para poder realizar esas actividades con una herramienta libre (y la mayoría de veces gratuita). Queremos advertir, sin embargo, que la usabilidad y el diseño de la interfaz en algunas de aplicaciones analizadas suele ser menos intuitiva y atractiva que su homónima comercial.

Y es que, amigas y amigos, optar por una solución ética y transparente, que sabemos lo que hace o que al menos tenemos la posibilidad de auditar, a día de hoy implica realizar un sacrificio. Este es pequeño porque una vez convivimos con esas aplicaciones resulta fácil adaptarnos a ellas, aunque al principio son tan sencillas y cómodas como lo puedan ser el conjunto de herramientas por las que vendemos nuestra alma al diablo (y nuestra investigación, si es que no es lo mismo). Esto en ningún caso es una crítica a las alternativas en sí mismas, que además en reseñables ocaciones funcionan mejor que las plataformas comerciales (nada tiene que envidiar Jitsi a Zoom o Skype, ni Framadate a Doodle, ni mucho menos Python o R a SPSS). Al contrario, queremos plantear que unas herramientas sólidas no nacerán por generación espontánea, sino por una masa crítica que las emplea y apoya de diversas formas. 

Las comunidades de cultura libre hacen un esfuerzo tremendo en construir plataformas de todo tipo que sean éticas con nuestros datos, también con la información de nuestras investigaciones y los de las personas con quienes nos comunicamos. Sin  embargo, nadan contra la corriente de un mercado cada vez más dominado por las grandes compañías tecnológicas, que cuentan con el capital económico y humano para apropiarse y desarrollar las opciones de software más usables y populares. Debemos tener en cuenta que optar por una comodidad inmediata puede ser conveniente a corto plazo, pero puede traernos problemas en un futuro no tan lejano como pueda parecer (¿O acaso no nos hemos llevado últimamente las manos a la cabeza cuando oíamos hablar de Cambridge Analytica?). Al final, utilizar los programas que nos han regalado o incluso enseñado en nuestra formación más básica es lo sencillo y cómodo. Pero estamos pagando un coste que va mucho más allá del dinero: el de la autonomía, la transparencia y la privacidad de nuestras investigaciones.

Y permitidnos que, desde estas líneas tiremos un guante a la administración de turno, pues resulta paradójico que se financie públicamente la ciencia (cuando eso ocurre) pero que no se ofrezcan herramientas libres que permitan la colaboración en sus investigaciones y que todo esto se encuentre alojado en servidores públicos, protegidos con la seguridad que se merecen. No solo porque ello favorecería a la transparencia y replicabilidad de los estudios, sino porque no tiene sentido que el código de financiación pública no beneficie al común de la población, como elocuentemente explica la carta Public Money, Public Code ([https://publiccode.eu/openletter/)](https://publiccode.eu/openletter/). Esta solución es incluso menos costosa que la opción privativa y, ante todo, protege los intereses de las investigaciones y de las personas que investigan. Es necesario apostar por una serie de aplicaciones web de código libre alojadas en un servidor público. 

Aprender a programar, apropiarse del código, es lo que realmente nos hace libres. Pero no podemos esperar que en un mundo tan complejo e hiperespecializado todas las personas diseñen y auditen código. Por suerte, el apoyo al software libre no ha de limitarse a su creación y perfeccionamiento posterior: podemos donar a nuestros proyectos favoritos, demandar a nuestras universidades que los incluyan en sus repositorios, enseñar a utilizarlos en las aulas. Por nuestra parte, esperamos que este pequeño manual sea una contribución más a los esfuerzos por una investigación social libre.

Imagínate una facultad o centro de investigación con las ventanas negras. En su interior los pasillos están en silencio estrechos y las puertas permanecen cerradas: el conocimiento no fluye, no se conecta, se crea en salas cerradas a las que solo se puede entrar bajo la condición de pagar un precio por la entrada. En ese centro o facultad el conocimiento fluiría con dificultad y se parecía mucho al software privativo, que no permite conocer el código y en la mayoría de las ocasiones (con permiso del pirateo), es necesario comprar una licencia para poder emplearlo. Las alternativas que se presentan aquí cumplen la mayoría con las necesidades que tiene el personal investigador de ciencias sociales. Estas alternativas han sido diseñadas precisamente para poder realizar esas actividades con una herramienta libre -y la mayoría de veces gratuita-. Sin embargo, la usabilidad y el diseño de la interfaz, en la mayoría de aplicaciones analizadas, suele ser menos intuitiva y atractiva que su homónima comercial.


### **Licencias: acrónimos e información**

Bajo el objetivo En las próximas fichas aparecen los acrónimos de las licencias a través de las cuales se distribuyen los programas sugeridos. En esta sección te ofrecemos más enlaces para ampliar el conocimiento sobre cada una de ellas. 



#### **AGPL: Affero General Public License**

🔗 [https://www.gnu.org/licenses/agpl-3.0.en.html](https://www.gnu.org/licenses/agpl-3.0.en.html) 



**Apache: Apache License**

🔗 [https://www.apache.org/licenses/](https://www.apache.org/licenses/)



#### **BSD: Berkeley Software Distribution**

🔗 [https://www.freebsd.org/doc/es/articles/explaining-bsd/article.html](https://www.freebsd.org/doc/es/articles/explaining-bsd/article.html)



#### **CC: Creative Commons**

🔗 [https://creativecommons.org](https://creativecommons.org)



#### **CeCILL:  CEA CNRS INRIA Logiciel Libre**

🔗 [https://cecill.info/licences/Licence\_CeCILL-B\_V1-en.html](https://cecill.info/licences/Licence\_CeCILL-B\_V1-en.html)



#### **CPAL: Common Public Attribution License**

🔗 [https://opensource.org/licenses/CPAL-1.0](https://opensource.org/licenses/CPAL-1.0)



#### **EUPL: European Union Public Licence**

🔗 [https://www.eupl.eu/](https://www.eupl.eu/)



#### **GPL: GNU General Public** **License** 

🔗 [https://www.gnu.org/licenses/old-licenses/gpl-1.0.html](https://www.gnu.org/licenses/old-licenses/gpl-1.0.html)



**MIT: MIT License**

🔗 [https://mit-license.org/](https://mit-license.org/)



**MPL: Mozilla Public License**

🔗 [https://www.mozilla.org/en-US/MPL/](https://www.mozilla.org/en-US/MPL/)











## **1. EXTRACCIÓN Y ANÁLISIS DE DATOS**
## Software de análisis cuantitativo

#### **Libre Office (Calc)**

Programa que permite leer y crear hojas de cálculo. Además de fórmulas matemáticas y estadísticas, también incorpora la posibilidad de crear gráficos y dar formatos editables a las tablas.



💻 Aplicación instalable en GNU/Linux, MacOS X y Windows

Licencia: MPL

Alternativa a: Excel



🔗 Página del programa: [https://www.libreoffice.org/discover/calc/](https://www.libreoffice.org/discover/calc/)

🔗 Repositorio: [https://www.libreoffice.org/download/download/](https://www.libreoffice.org/download/download/)

🔗 Foro de ayuda: [https://wiki.documentfoundation.org/Main\_Page](https://wiki.documentfoundation.org/Main\_Page)

    

🗺️ Català/Valencià, Castellano/Español, Galego, English





#### **EtherCalc**

Hoja de cálculo online que permite el trabajo colaborativo en tiempo real. No se necesita una cuenta de usuario y permite descargar/exportar el contenido a .ods, .html, .csv y .xlsx



💻🌐 Aplicación web, no necesita ser instalada, pero sí que tiene la opción de trabajar en modo local instalándola. En ese caso, es instalable en GNU/Linux, FreeBSD, MacOS X y Windows.

Licencia: CPAL

Alternativa a: Google Sheets



🔗 Página del programa: [https://ethercalc.net/](https://ethercalc.net/)

🔗 Repositorio: [https://github.com/audreyt/ethercalc](https://github.com/audreyt/ethercalc)



🗺️ English





#### **PSPP**

Herramimenta profesional de cálculo estadístico que permite trabajar con grandes conjuntos de datos.



💻 Aplicación instalable en Linux, MacOS GNU/y Windows.

Licencia: GPL

Alternativa a: SPSS



🔗 Página del programa: [https://www.gnu.org/software/pspp/](https://www.gnu.org/software/pspp/)

🔗 Repositorio: [http://ftp.rediris.es/mirror/GNU/pspp/](http://ftp.rediris.es/mirror/GNU/pspp/)

🔗 Foro de ayuda: [https://lists.gnu.org/mailman/listinfo/pspp-users](https://lists.gnu.org/mailman/listinfo/pspp-users)

    

🗺️ Se adapta al idioma que tenga establecido el entorno de usuario del ordenador.

#### 



**R**

Es un lenguaje de programación que permite realizar cálculos estadísticos complejos. Existe una gran comunidad de colaboradores, por lo que constantemente hay nuevos paquetes con nuevas funcionalidades disponibles y actualizadas. Es una herramienta óptima tanto para el análisis como para la recolección de contenido web (web scraping). Es necesario conocer el lenguaje para poder operar con este programa.



💻 Aplicación instalable en GNU/Linux, Mac OS X y Windows

Licencia: GPL

Alternativa a:  SPSS, Google Sheets, Microsoft Excel



🔗 Página del programa: [https://www.r-project.org/](https://www.r-project.org/)

🔗 Foro de ayuda: [https://cran.r-project.org/manuals.html](https://cran.r-project.org/manuals.html)



🗺️ English





#### **Python**

Es un lenguaje de programación que permite realizar cálculos estadísticos complejos. Existe una gran comunidad de colaboradores, por lo que constantemente hay nuevos paquetes con nuevas funcionalidades disponibles y actualizadas. Es una herramienta óptima tanto para el análisis como para la recolección de contenido web (web scraping). Es necesario conocer el lenguaje para poder operar con este programa.



💻 Aplicación instalable en GNU/Linux, Mac OS X y Windows

Licencia: Python License, la mayoría de sus versiones son GPL-compatible

Alternativa a: SPSS, Google Sheets, Microsoft Excel



🔗 Página del programa: [https://www.python.org/](https://www.python.org/)

🔗 Foro de ayuda: [https://www.python.org/about/help/](https://www.python.org/about/help/)



🗺️ English





## Software de análisis cualitativo

#### **Aquad**

Permite el análisis cualitativo y cuantitativo de documentos de distinta naturaleza: texto, imágenes, audio, vídeo. Se puede codificar sin contar con una transcripción, etiquetando directamente la imagen.



💻 Aplicación instalable en Windows, Linux y MacOS

Licencia: GPL v3

Alternativa a: Nvivo, Atlas.ti



🔗 Página del programa: [http://www.aquad.de/en/](http://www.aquad.de/en/)

🔗 Foro de ayuda: [http://www.aquad.de/en/support/#BeratungCoaching](http://www.aquad.de/en/support/#BeratungCoaching)



🗺️ Castellano/Español, English

    

    

#### **Taguette**

Herramienta para el análisis cualitativo de documentos de texto (.pdf, .txt, .docx, .html, .odt, .rtf). Permite subrayar palabras o frases y etiquetarlas con los códigos que el usuario configura según su criterio. Una vez finalizada la codificación del corpus, permite ver y exportar los resultados derivados de cada código en distintos formatos.



💻🌐 Aplicación instalable en GNU/Linux, Windows y Mac OS X . También permite trabajar de forma colaborativa registrádose en la plataforma y trabajando en su servidor (aplicación web).

Licencia: BSD

Alternativa a: Nvivo, Atlas.ti



🔗 Página del programa: [https://www.taguette.org/](https://www.taguette.org/)

🔗 Repositorio: [https://gitlab.com/remram44/taguette](https://gitlab.com/remram44/taguette)

🔗 Foro de ayuda: [https://www.taguette.org/getting-started.html](https://www.taguette.org/getting-started.html)



🗺️ English





#### **RQDA**

Paquete adicional de R que permite el análisis cualitativo de documentos de texto a partir de la aplicación de etiquetas (códigos).



💻 Aplicación instalable en Windows, Mac OS y Linux, a partir de R. Es necesario tener R instalado, al tratarse de una extensión del programa.

Licencia: BSD

Alternativa a: Nvivo / Atlas.ti



🔗 Página del programa: [http://rqda.r-forge.r-project.org/](http://rqda.r-forge.r-project.org/)

🔗 Foro de ayuda: [https://lists.r-forge.r-project.org/cgi-bin/mailman/listinfo/rqda-help](https://lists.r-forge.r-project.org/cgi-bin/mailman/listinfo/rqda-help)



🗺️   English





## Gráficos y diagramas

#### Rawgraphs

Herramienta para crear representaciones gráficas de bases de datos. Se pueden pegar directamente los datos en la web o subir un archivo .csv. Permite además exportar en .png y en .svg, lo que facilita editar después en vectorial el resultado obtenido para terminar de perfeccionar el diseño de la figura.



🌐 Aplicación web

Licencia: Apache V2

Alternativa a: Tableau



🔗 Página del programa: [https://rawgraphs.io/](https://rawgraphs.io/)

🔗 Repositorio: [https://github.com/rawgraphs/raw/](https://github.com/rawgraphs/raw/)

🔗 Foro de ayuda: [https://github.com/densitydesign/raw](https://github.com/densitydesign/raw)



🗺️   English

    

#### **Librería de Python: Bokeh **

Extensión instalable de Python que permite visualizar de forma gráfica bases de datos. Tiene una librería de visualizaciones muy atractivas y con posibilidad de ser interactivas.



💻 Aplicación instalable en Windows, Mac OS y Linux

Licencia: BSD 3

Alternativa a: Visualizaciones de SPSS u otro tipo de software estadístico





🔗 Página del programa: [https://docs.bokeh.org/en/latest/](https://docs.bokeh.org/en/latest/)

🔗 Repositorio:  [https://github.com/bokeh/bokeh](https://github.com/bokeh/bokeh)

🔗 Documentación: [https://docs.bokeh.org/en/latest/docs/user\_guide.html](https://docs.bokeh.org/en/latest/docs/user\_guide.html)



🗺️ English 



## Encuestas 

#### **Limesurvey**

Herramienta para crear y distribuir cuestionarios online. Permite realizar distintos formatos de preguntas.



🌐 Aplicación web (no necesita ser instalada)

Licencia: GPL

Alternativa a: Google Drive (Forms), Typeform, SurveyMonkey

💸 Freemium



🔗 Página del programa:  [https://www.limesurvey.org/es/](https://www.limesurvey.org/es/)

🔗 Repositorio: [https://github.com/LimeSurvey/LimeSurvey](https://github.com/LimeSurvey/LimeSurvey)

🔗 Foro de ayuda: [https://manual.limesurvey.org/LimeSurvey\_Manual](https://manual.limesurvey.org/LimeSurvey\_Manual)



🗺️ Se adapta al idioma del sistema





#### **SurveyJS**

Herramienta para crear y distribuir encuestas online. En este caso, se instala y se monta en el propio servidor. Óptima para tener un control total de los datos. 



🌐 Aplicación web, puede ser ejecutada por el usuario a partir de código

Licencia: MIT

Alternativa a: Google Drive (Forms), Typeform, SurveyMonkey

💸 Esta aplicación sólo es gratuita si se gestiona personalmente todo,  si se quiere utilizar como una aplicación web externa (servidor ajeno) sí que tiene un coste en función del espacio requerido.



🔗 Página del programa: [https://surveyjs.io/](https://surveyjs.io/)

🔗 Repositorio: [https://github.com/surveyjs/survey-library](https://github.com/surveyjs/survey-library)

🔗 Foro de ayuda: [https://surveyjs.answerdesk.io/ticket/list](https://surveyjs.answerdesk.io/ticket/list)



🗺️ English





#### **OhMyForm**

Herramienta online que permite crear cuestionarios online. El usuario lo ejecuta a partir del código y, de este modo, configura el servidor en el que quiere almacenar las respuestas.



🌐 Aplicación web, necesita ser instalado por el usuario a partir del código

 Licencia: AGPL V.3

 Alternativa a: Google Drive (Forms), Typeform



🔗 Página del programa: [https://ohmyform.com/](https://ohmyform.com/)

🔗 Repositorio: [https://github.com/ohmyform/ohmyform](https://github.com/ohmyform/ohmyform)

🔗 Foro de ayuda: [https://discord.com/invite/3jYMAYg](https://discord.com/invite/3jYMAYg)



🗺️ English





#### **EUSurvey**

Cuenta con la posibilidad de crear consultas públicas. Permite la posibilidad de crear distintos tipos de preguntas (abiertas, de respuesta múltiple, etc.). Está diseñada y mantenida por la Dirección General de Informática de la Comisión Europea.



🌐 Aplicación web (no necesita ser instalada)

Licencia: EUPL

Alternativa a:  Google Drive (Forms), Typeform, SurveyMonkey



🔗 Página del programa: [https://ec.europa.eu/eusurvey/home/welcome](https://ec.europa.eu/eusurvey/home/welcome)

🔗 Repositorio: [https://joinup.ec.europa.eu/solution/eusurvey](https://joinup.ec.europa.eu/solution/eusurvey)

🔗 Foro de ayuda: [https://ec.europa.eu/eusurvey/home/documentation](https://ec.europa.eu/eusurvey/home/documentation)



🗺️ Castellano/Español

    

    

## Mapas

#### **Ushahidi**

Herramienta de geolocalización que permite editar de forma colaborativa un mapa, creando puntos de interés y pudiendo añadir información relevante sobre él. Es una plataforma adecuada mapear redes de colaboración o visibilizar determinados problemas. Una herramienta que se puede adaptar a proyectos colaborativos de ciencia ciudadana.



🌐 Aplicación web (no necesita ser instalada)

Licencia: AGPL

Alternativa a: Google Maps





🔗 Página del programa: [https://www.ushahidi.com/](https://www.ushahidi.com/)

🔗 Repositorio: [https://github.com/ushahidi/platform](https://github.com/ushahidi/platform)

🔗 Foro de ayuda: [https://docs.ushahidi.com/ushahidi-platform-user-manual/](https://docs.ushahidi.com/ushahidi-platform-user-manual/)



🗺️ English





#### **QGIS**

Herramienta que permite editar, visualizar y publicar información geográfica.



💻📱 Aplicación instalable en GNU/ Linux, Mac OS X, Windows, Android e iOS

Licencia: GPL v2.0

Alternativa a: ArcGIS



🔗 Página del programa: [https://www.qgis.org/es/site/](https://www.qgis.org/es/site/)

🔗 Repositorio: [https://github.com/qgis/QGIS](https://github.com/qgis/QGIS)

🔗 Foro de ayuda: [https://www.qgis.org/es/site/forusers/support.html](https://www.qgis.org/es/site/forusers/support.html)



🗺️ English





## Transcriptor de entrevistas y audio 

#### **oTranscribe**

No es una herramienta de transcripción automática. Es una interfaz que facilita la escritura manual de la transcripción.



🌐 Aplicación web (no necesita ser instalada)

Licencia: MIT

Alternativa a: Express Scribe



🔗 Página del programa: [https://otranscribe.com/](https://otranscribe.com/)

🔗 Repositorio: [https://github.com/oTranscribe/oTranscribe](https://github.com/oTranscribe/oTranscribe)

🔗 Foro de ayuda: [https://otranscribe.com/help/](https://otranscribe.com/help/)



🗺️ Castellano/Spanish, Català/Valencià







## **3. OFIMÁTICA Y PRESENTACIÓN DE CONTENIDO**
## Procesadores de texto

#### **LibreOffice (Writer)**

Herramienta para editar documentos de texto. Incorpora un autocorrector para revisar la gramática y la ortografía. Permite añadir imágenes y gráficos, hecho que también posibilita visualizar el diseño final del documento. 



💻 Aplicación instalable en GNU/Linux, Mac OS X y Windows

Licencia: MPL v2.0.

Alternativa a: Microsoft Word, Pages



🔗 Página del programa: [https://www.libreoffice.org/discover/writer/](https://www.libreoffice.org/discover/writer/)

🔗 Repositorio: [https://www.libreoffice.org/download/download/](https://www.libreoffice.org/download/download/)

🔗 Foro de ayuda: [https://www.libreoffice.org/discover/writer/](https://www.libreoffice.org/discover/writer/)



🗺️ Català/Valencià, Castellano/Español, Galego, English

    

    

#### **Cryptpad**

Es un entorno que posibilita la creación y trabajo en colaboración de distintos tipos de documentos: pad (documento de texto online), código, presentación, hoja de cálculo, encuesta, kanban, pizarra y nube. En el caso que nos ocupa aquí, el pad, tiene un diseño muy intuitivo y permite escribir y dar formato al texto. El único inconveniente es que no permite incorporar imágenes. Pero permite exportar a word y en formato .html.  Asimismo, este entorno se caracteriza respecto a otros por el valor que otorga a la privacidad del usuario. Esta herramienta es ideal para trabajos colaborativos.



🌐  Aplicación web (no necesita ser instalada)

Licencia: GPL

Alternativa a: Google Docs

💸 Freemium (Sin necesidad de tener una cuenta puedes iniciar un pad y compartirlo)



🔗 Página del programa: [https://cryptpad.fr/](https://cryptpad.fr/)

🔗 Repositorio: [https://github.com/xwiki-labs/cryptpad/](https://github.com/xwiki-labs/cryptpad/)



🗺️  Castellano/Español, Català/Valencià, English





## Documentación Portable PDF (lectura y edición)

#### **Okular**

Permite visualizar y realizar comentarios en documentos .pdf.



💻 Aplicación instalable en GNU/Linux, Mac OS X y Windows

Licencia: GPL-2.0+ y GFDL-1.3

Alternativa a: Adobe Reader



🔗 Página del programa: [https://okular.kde.org/](https://okular.kde.org/)

🔗 Repositorio: [https://cgit.kde.org/okular.git](https://cgit.kde.org/okular.git)

🔗 Foro de ayuda: [https://forum.kde.org/](https://forum.kde.org/)



🗺️  English

    

    

#### **Firefox PDF Viewer (PDF.js)**

Extensión para Firefox y Chrome que permite visualizar los .pdf en el navegador. La instalación se ejecuta desde el ordenador, no desde el propio navegador.



💻 Aplicación instalable en GNU/Linux, Mac OS X y Windows

Licencia: CC

Alternativa a: Adobe Reader



🔗 Página del programa: [https://mozilla.github.io/pdf.js/](https://mozilla.github.io/pdf.js/)

🔗 Repositorio: [https://github.com/mozilla/pdf.js](https://github.com/mozilla/pdf.js)

🔗 Foro de ayuda: [https://github.com/mozilla/pdf.js/wiki](https://github.com/mozilla/pdf.js/wiki)



🗺️  English





## **4. Presentaciones**

#### **Libre office (Impress)**

Herramienta para realizar presentaciones en las que se puede crear tanto texto como elementos gráficos para diseñar la presentación. Permite incluir imágenes, audio y vídeo, así como efectos para realizar las transciones de una diapositiva a otra.



💻 Aplicación instalable en GNU/Linux, Mac OS X y Windows

Licencia: MPL

Alternativa a: Power Point



🔗 Página del programa: [https://www.libreoffice.org/discover/impress/](https://www.libreoffice.org/discover/impress/)

🔗 Repositorio: [https://www.libreoffice.org/download/download/](https://www.libreoffice.org/download/download/)

🔗 Foro de ayuda: [https://wiki.documentfoundation.org/Main\_Page](https://wiki.documentfoundation.org/Main\_Page)



🗺️ Català/Valencià, Galego

    

    

#### **Sozi**

Es una herramienta que permite realizar presentaciones animadas. En lugar de la clásica estructura de presentación a partir de diapositivas, esta presentación se caracteriza por estructurarse como si de un gran póster se tratara y las transiciones consistieran en movimientos en el espacio y en acercamientos (zooms) por el documento. Se combina con la edición de gráficos vectoriales.



💻 Aplicación instalable en GNU/Linux, Mac OS X y Windows. Hay que instalarlo a partir del código, no hay instalador disponible en la web

Licencia: CC, MPL

Alternativa a: Prezi



🔗 Página del programa: [https://sozi.baierouge.fr/](https://sozi.baierouge.fr/)

🔗 Repositorio: [https://github.com/senshu/Sozi](https://github.com/senshu/Sozi)

🔗 Foro de ayuda: [https://sozi.baierouge.fr/pages/60-community.html](https://sozi.baierouge.fr/pages/60-community.html)



🗺️ English





## Editor de imágenes

#### **GIMP (GNU Image Manipulation Program)**

Herramienta de edición de imagen que permite utilizar el sistema de edición por capas. Permite el retoque fotográfico con una variedad de herramientas profesionales y es muy útil para retocar fotografías, imágenes y gráficos. Existe un extenso catálogo de plug ins para aumentar la opciones de edición.



💻 Aplicación instalable en GNU/Linux, Mac OS X y Windows

Licencia: CC

Alternativa a: Adobe Photoshop



🔗 Página del programa: [https://www.gimp.org/](https://www.gimp.org/)

🔗 Repositorio: [https://github.com/GNOME/gimp](https://github.com/GNOME/gimp)

🔗 Foro de ayuda: [https://wiki.gimp.org/index.php/Main\_Page](https://wiki.gimp.org/index.php/Main\_Page)



🗺️ Se adapta al idioma del sistema. Puede cambiarse de forma manual.



    

#### **InkScape**

Herramienta profesional para editar elementos gráficos vectoriales, crear ilustraciones y componer varios elementos gráficos. Trabaja con archivos .svg, .ai, .eps, .pdf, .ps y .png.  Este software está centrado en el diseño gráfico, por lo que se recomienda mucho más su uso para el diseño de cartelería, gráficos (figuras de un estudio), trípticos, logos, tarjetas, etc. 



💻 Aplicación instalable en GNU/Linux, MacOS X, Windows 

Licencia: GPL

Alternativa a: Adobe Illustrator



🔗 Página del programa: [https://inkscape.org/](https://inkscape.org/)

🔗 Repositorio: [https://gitlab.com/inkscape/inkscape](https://gitlab.com/inkscape/inkscape)

🔗 Foro de ayuda: [https://inkscape.org/forums/questions/](https://inkscape.org/forums/questions/)



🗺️  Castellano/Español, English





## Editor de vídeo

#### **Kdenlive**

Editor de vídeo multipista, con librería de transiciones y efectos y capacidad para diseñar títulos 2D. Permite instalar plug-ins para aumentar su librería de efectos y opciones de edición. Los efectos pueden modularse a partir de keyframes, lo que aporta un mayor control del proceso de edición.



💻 Aplicación instalable en GNU/Linux, BSD, Mac OS X y  Windows. En el caso de Mac OS X necesita instalarse a partir del código, no está actualizada la versión compilada para instalar.

Licencia: GPL

Alternativa a: Adobe Premiere, Final Cut



🔗 Página del programa: [https://kdenlive.org/es/](https://kdenlive.org/es/)

🔗 Repositorio: [https://cgit.kde.org/kdenlive.git](https://cgit.kde.org/kdenlive.git)

🔗 Foro de ayuda:  [https://forum.kde.org/viewforum.php?f=262](https://forum.kde.org/viewforum.php?f=262)



🗺️  Castellano/Español, English

    

    

#### **Shotcut**

Herramienta de edición de vídeo no lineal multipista. Permite tener un control preciso de los clips de video y soporta archivos de múltiples formatos (incluido 4K). Incluye efectos de vídeo y de edición, pero también se pueden ampliar a partir de plug ins. Consigue resultados profesionales. En la web oficial se puede encontrar información para crear incluso los propios plugins.



💻📱 Aplicación instalable en Windows, Mac OS X y GNU/Linux

Licencia: GPL v3

Alternativa a: Adobe Premiere, Final Cut



🔗 Página del programa: [http://www.shotcut.org/](http://www.shotcut.org/)

🔗 Repositorio: [https://github.com/mltframework/shotcut](https://github.com/mltframework/shotcut)

🔗 Foro de ayuda:  [https://forum.shotcut.org/](https://forum.shotcut.org/)

    

🗺️  Castellano/Español, Català/Valencià, English, Galego 





## Maquetación

#### **Scribus**

Herramienta profesional para maquetar publicaciones. Permite diseñar visualmente la composición de textos e imágenes en páginas y exportarlas en formato .pdf. Útil para maquetar artículos, revistas, calls for papers, actas de congreso, etc. con acabado profesional.



💻 Aplicación instalable en GNU/Linux, BSD UNIX, Solaris, OpenIndiana, GNU/Hurd, OS/2, Mac OS X y Windows

Warp 4, eComStation

Licencia: CC

Alternativa a: Adobe InDesign, QuarkXPress



🔗 Página del programa: [https://www.scribus.net/](https://www.scribus.net/)

🔗 Repositorio: [https://sourceforge.net/projects/scribus/](https://sourceforge.net/projects/scribus/)

🔗 Foro de ayuda:  [https://wiki.scribus.net/canvas/Scribus](https://wiki.scribus.net/canvas/Scribus)



🗺️  Castellano/Español, English





#### **InkScape**

Herramienta profesional para editar elementos gráficos vectoriales, crear ilustraciones y componer varios elementos gráficos. Trabaja con archivos .svg, .ai, .eps, .pdf, .ps y .png.  Este software está centrado en el diseño gráfico, por lo que se recomienda mucho más su uso para el diseño de cartelería, gráficos (figuras de un estudio), trípticos, logos, tarjetas, etc. Para publicaciones con mucho texto se recomienda utilizar Scribus.



💻 Sistemas operativos para los que está disponible: GNU/Linux, Mac OS X y Windows

Licencia: GPL

Alternativa a: Adobe Illustrator



🔗 Página del programa: [https://inkscape.org/](https://inkscape.org/)

🔗 Repositorio: [https://gitlab.com/inkscape/inkscape](https://gitlab.com/inkscape/inkscape)

🔗 Foro de ayuda:  [https://inkscape.org/forums/questions/](https://inkscape.org/forums/questions/)



🗺️ Castellano/Español, English

    

    

## Traductor

#### **Apertium**

Herramienta que traduce de forma automática textos, documentos y páginas web. Reconoce 46 idiomas para traducir, aunque no todos están disponibles entre ellos.



💻📱🌐 Aplicación web (no necesita ser instalada). Posibilidad de instalar en cualquier dispositivo (móvil u ordenador) a partir del código ([http://wiki.apertium.org/wiki/Installation)](http://wiki.apertium.org/wiki/Installation)

Licencia: CC, GPL

Alternativa a: Google Translate



🔗 Página del programa: [http://www.apertium.org/index.spa.html?dir=arg-cat#translation](http://www.apertium.org/index.spa.html?dir=arg-cat#translation)

🔗 Repositorio: [https://sourceforge.net/projects/apertium/files/](https://sourceforge.net/projects/apertium/files/)

🔗 Foro de ayuda:  [http://wiki.apertium.org/wiki/Documentation](http://wiki.apertium.org/wiki/Documentation)



🗺️ Aragonés, Castellano/Español, Català/Valencià, Euskera, Galego





#### **Tatoeba**

Herramienta para buscar la traducción de palabras y oraciones simples. No es un traductor automático, es una base de datos con traducciones en múltiples idiomas elaboradas por la comunidad. Útil para buscar palabras específicas.



🌐 Aplicación web (no necesita ser instalada)

Licencia: CC

Alternativa a: Lingüee



🔗 Página del programa: [https://tatoeba.org/spa/](https://tatoeba.org/spa/)

🔗 Repositorio: [https://github.com/Tatoeba/tatoeba2](https://github.com/Tatoeba/tatoeba2)

🔗 Foro de ayuda:  [https://groups.google.com/forum/#](https://groups.google.com/forum/#)!forum/tatoebaproject



🗺️ Castellano/Español, English





## **2. COMUNICACIÓN Y TRABAJO EN EQUIPO**

### **Transferencia de ficheros**

#### **Syncthing**

Herramienta para la sincronización de archivos entre dispositivos. El programa crea repositorios con ruta a una carpeta local, donde se añaden los documentos de cualquier tipo que se deseas compartir. Posteriormente, tendrás que incluir los nodos a los otros equipos donde se sincronizará dicha carpeta. El programa ofrece la opción de crear repositorios no editables, para impedir que el resto de los nodos participantes puedan añadir o eliminar contenido.



💻📱 Aplicación instalable en GNU/Linux, FreeBSD, OpenBSD, NetBSD, Dragonfly BSD, Illumos, Solaris, Mac OS X, Windows

Licencia: MPL

Alternativa a: Bittorrent Sync, iCloud, Google One, Dropbox



🔗 Página del programa: [https://syncthing.net/](https://syncthing.net/)

🔗 Repositorio: [https://forum.syncthing.net/](https://forum.syncthing.net/)



🗺️ Castellano/Español, English





#### **Firefox Send**

Aplicación web (también instalable en Android) para compartir archivos de hasta 2,5 gigabytes con cifrado de extremo a extremo. Cuando se sube el archivo, genera un enlace de descarga. Además, es posible añadir una contraseña para poder abrir el link, así como una caducidad por días o número de descargas.



📱🌐 Aplicación offline instalable: Android y web (no necesita ser instalada)

Licencia: MPL

Alternativa a: WeTransfer, pCloud



🔗 Página del programa: [https://send.firefox.com/](https://send.firefox.com/) 

🔗 Repositorio [https://github.com/mozilla/send](https://github.com/mozilla/send)

🔗 Foro de ayuda: [https://qsurvey.mozilla.com/s3/Firefox-Send-Product-Feedback?ver=3.0.21\&browser=firefox](https://qsurvey.mozilla.com/s3/Firefox-Send-Product-Feedback?ver=3.0.21\&browser=firefox)



🗺️ Castellano/Español, English





#### **Freenet** 

Plataforma de redes de pares para compartir archivos. Una vez el documento es subido a la red, este queda ya incluido en la red descentralizada de nodos, de manera que no es preciso dicho nodo continúe disponible para que el documento siga disponible. Existen Freesites que categorizan el contenido subido a Freenet. También permite la comunicación anónima a través de chats y foros



💻📱🌐 Aplicación instalable en: GNU/Linux, Mac OS X y Windows

Licencia: GPL



🔗 Página del programa: [https://freenetproject.org/](https://freenetproject.org/)

🔗 Repositorio: [https://github.com/freenet/fred](https://github.com/freenet/fred) 

🔗 Foro de ayuda: [https://freenetproject.org/pages/help.html](https://freenetproject.org/pages/help.html)



🗺️ Castellano/Español, English





### **Cliente de correo electrónico**

#### Mozilla Tunderbird

Cliente de correo electrónico que incluye también para diferentes cuentas. También te permite chatear si cuentas con un perfil en algún proveedor de mensajería instantánea (IRC, XMPP, Twitter y Google Talk) y leer noticias o blog a través de la suscripción RSS.



💻 Aplicación instalable en: GNU/Linux, Mac OS y Windows

Licencia: MPL

Alternativa a: Microsoft Office Outlook



🔗 Página del programa: [https://www.thunderbird.net/](https://www.thunderbird.net/)

🔗 Repositorio: [https://github.com/mozilla-comm](https://github.com/mozilla-comm)

🔗 Foro de ayuda: [https://support.mozilla.org/es/products/thunderbirda](https://support.mozilla.org/es/products/thunderbirda)



🗺️ Asturianu, Castellano/Español, Català/Valencià, English, Euskera, Galego





#### Disroot

Servicio de correo electrónico. Una vez aceptan (manualmente) una solicitud para el uso de sus servicios, además de contar con una cuenta de Disroot se pueden emplear el resto de sus herramientas y utilidades, que incluyen encuestas, pads, nube, etc. 



🌐 Aplicación web (no necesita ser instalada)

Licencia: CC 4.0

Gmail, Outlook (y casi cualquier programa que puedas necesitar).



🔗 Página del programa: [https://disroot.org/es](https://disroot.org/es)

🔗 Foro de ayuda: [https://howto.disroot.org/es/](https://howto.disroot.org/es/)



🗺️ Catellano/Español, English





#### ProtonMail

Servicio de correo electrónico cifrado con su aplicación web con servidores en Suiza (y, por tanto, fuera de la jurisdicción de Estados Unidos y la Unión Europea).



🌐 Aplicación web (no necesita ser instalada)

Licencia:  MIT

Alternativa a: Gmail, Outlook



🔗 Página del programa: [https://protonmail.com](https://protonmail.com)

🔗 Repositorio: [https://github.com/ProtonMail](https://github.com/ProtonMail)

🔗 Foro de ayuda: [https://protonmail.uservoice.com/forums/284483-feedback](https://protonmail.uservoice.com/forums/284483-feedback)



🗺️ Castellano/Español, English





#### Tutanota

Servicio de correo electrónico cifrado que ofrece también correo electrónico cifrado. 



🌐 Aplicación web (no necesita ser instalada)

Licencia: GPL

Alternativa a:  Gmail, Outlook

💸 Cuenta con un plan gratuito y varios de pago, dependiendo de la contratación de servicios concretos ([https://www.tutanota.com/es/pricing)](https://www.tutanota.com/es/pricing)) 



🔗 Página del programa: [https://www.tutanota.com/es/](https://www.tutanota.com/es/) 

🔗 Repositorio: [https://github.com/tutao/tutanota](https://github.com/tutao/tutanota)

🔗 Foro de ayuda: [https://www.tutanota.com/es/faq](https://www.tutanota.com/es/faq) 



🗺️ Castellano/Español, Català/Valencià, Galego, English





### **Videoconferencia**

#### Jitsi

Programa de videoconferencia que no necesita instalación en el ordenador. Es posible acceder a una sala mediante un enlace público en tu navegador, grabar la pantalla de la conversación y añadir una contraseña para proteger las reuniones. Permite la retransmisión de la conversación a través de YouTube.



📱🌐 Aplicación offline instalable en Android, iOS y aplicación web (no necesita ser instalada)

Licencia: Apache 

Alternativa a: Skype, Zoom



🔗 Página del programa: [https://jitsi.org/](https://jitsi.org/)

🔗 Repositorio: [https://github.com/jitsi](https://github.com/jitsi)

🔗 Foro de ayuda: [https://jitsi.org/user-faq/](https://jitsi.org/user-faq/)



🗺️ Castellano/Español, English





#### Mumble

Aplicación de videoconferencia de baja latencia. Facilita la creación de canales que se pueden enlazar con otros, de modo que se pueden escuchar anuncios más generales mientras se mantienen conversaciones en canales con menor número de personas. 



💻📱 Aplicación instalable en GNU/Linux, Mac OS, Windows, Android e iOS, 

Licencia: BSD

Alternativa a: Discord, Zoom

💸 Su servidor principal se llama Murmur y da servicio a reuniones de hasta 50 usuarios



🔗 Página del programa: [https://www.mumble.info/](https://www.mumble.info/) 

🔗 Repositorio: [https://github.com/mumble-voip](https://github.com/mumble-voip)

🔗 Foro de ayuda: [https://wiki.mumble.info/wiki/Main\_Page](https://wiki.mumble.info/wiki/Main\_Page)



🗺️ Castellano/Español, English





### **Agenda y trabajo en equipo**

#### Framasoft

Iniciatia educativa francesa con más de 50 iniciativas específicas que se desarrollan en torno a los valores del software y la cultura libre, como tales como un repositorio de código, una compilación de videojuegos o un directorio de libros en abierto. Entre estas herramientas se encuentran dos relacionadas con la gestión y organización de tareas. Framagenda, por un lado, es un calendario que permite agregar tareas, eventos y recordatorios, así como gestionar una agenda de contactos.  Framadate, por otro, propone la creación de encuestas donde cada persona participante puede indicar los momentos en los que está disponible para realidar una reunión o asistir a un evento.



🌐 Aplicación web (no necesita ser instalada)

Licencia: CeCILL-B

Alternativa a: Google Calendar en el caso de Framagenda y Doodle en el de Framadate. El resto de aplicaciones suplen muchas otras aplicaciones que también puedas necesitar



🔗 Página del programa: [https://framasoft.org/en](https://framasoft.org/en)

🔗 Repositorio: [https://framagit.org/framasoft](https://framagit.org/framasoft)

🔗 Foro de ayuda: [https://docs.framasoft.org/en/](https://docs.framasoft.org/en/) / [https://framacolibri.org/](https://framacolibri.org/)



🗺️ Castellano/Español, English





#### GitLab

Repositorio de código basado en Git. En ella se puede alojar el código, modificarlo y controlar las nuevas versiones de este.



🌐 Aplicación web (no necesita ser instalada)

Licencia: MIT

Alternativa a: GitHub

💸 La versión básica es gratuita, mientras que otros planes oscilan entre los cuatro a los 99 dólares al mes ([https://about.gitlab.com/pricing/)](https://about.gitlab.com/pricing/))



🔗 Página del programa: [https://gitlab.com/gitlab-org/gitlab](https://gitlab.com/gitlab-org/gitlab)

🔗 Repositorio: [https://about.gitlab.com/](https://about.gitlab.com/)

🔗 Foro de ayuda: [https://docs.gitlab.com/](https://docs.gitlab.com/)



🗺️ Castellano/Español, English





#### Taiga

Plataforma para la gestión de proyectos de manea eficiente. Con esta herramienta se pueden plantear tareas y agruparlas, que luego podrás marcar como nuevas, en curso, listas para testear o cerradas. Además, también es posible añadir a personas que colaboren en ellas. Cuentan con wiki, videoconferencia y permite la integración de repositorios de código como GitLab o GitHub.



🌐 Aplicación web (no necesita ser instalada)

Licencia: AGPL

Alternativa a: Trello

💸 La versión grauita permite almacenar 300 megabytes. El siguiente plan tiene un coste de 5 dólares al mes. Cuentan con un precio especial para centros educativos y ONG de 1 dólar al mes ([https://taiga.io/pricing)](https://taiga.io/pricing))



🔗 Página del programa: [https://taiga.io/](https://taiga.io/)

🔗 Repositorio: [https://github.com/taigaio](https://github.com/taigaio)

🔗 Foro de ayuda: [https://tree.taiga.io/support/](https://tree.taiga.io/support/) / [https://groups.google.com/forum/#](https://groups.google.com/forum/#)!forum/taigaio



🗺️ Castellano/Español, English





#### Kanboard

Herramienta para la gestión de proyectos. Cuenta con una página principal con un tablero para la creación de estas, un listado del conjunto de tareas que han de seguirse, así como un calendario para poder organizarlas temporalmente. Se pueden compartir y distribuir tareas con otras personas usuarias y cuentan con visualizaciones para conocer la progresión del trabajo.



💻🌐 Aplicación instalable en GNU/Linux, Mac OS X, Windows y aplicación web (no necesita ser instalada)

Licencia: MIT 

Alternativa a: Trello



🔗 Página del programa: [https://kanboard.org/](https://kanboard.org/)

🔗 Repositorio:  [https://github.com/kanboard/kanboard](https://github.com/kanboard/kanboard)

🔗 Foro de ayuda:  [https://docs.kanboard.org/en/latest/user\_guide/](https://docs.kanboard.org/en/latest/user\_guide/)



🗺️ Castellano/Español, English





#### Riot.im

Cliente de mensajería instanánea que, además de la creación de chats, permite la transferencia de archivos e integra conferencias de voz y vídeo. También es posible enviar mensajes a varios usuarios mediante la creación de grupos específicos y mantener, así, varias conversaciones al mismo tiempo. 



💻📱🌐 Aplicación instalable en GNU/Linux, Mas OS, Windows, Android, iOS y web (no necesita ser instalada)

Alternativa a: Slack, Discord

Licencia: Apache 

💸 Es posible instalarlo en Matriz y otros servidores gratuitos ([https://about.riot.im/free)](https://about.riot.im/free))



🔗 Página del programa: [https://about.riot.im/](https://about.riot.im/)

🔗 Repositorio: [https://github.com/vector-im](https://github.com/vector-im)

🔗 Foro de ayuda: [https://about.riot.im/help](https://about.riot.im/help)



🗺️  Castellano/Español, Euskera, English





### **Cloud y bases de datos**

#### NextCloud

Programas para la creación de un servicio de alojamiento paralelo a OwnCloud. Las carpetas locales, por tanto, se sincronizan con el servidor y pueden compartirse mediante enlace. Adicionalmente, dispone de acceso a servicios como SQLite, de manera que se pueden configurar y gestionar una base de datos desde el propio servidor. 



💻📱 Aplicación instalable en GNU/Linux, Mac OS, Windows, Android e iOS

Licencia: AGPL

Alternativa a: Dropbox, Google Drive

💸 Ofrece servicio profesional cuyo plan más barato cuesta 1.900 euros al año ([https://nextcloud.com/pricing/)](https://nextcloud.com/pricing/))



🔗 Página del programa: [https://nextcloud.com/](https://nextcloud.com/)

🔗 Repositorio: [https://github.com/nextcloud](https://github.com/nextcloud)



🗺️ Català/Valencià, Castellano/Español, Galego, English, Euskera





### **Software de compresión**

#### 7-ZIP

Archivador de comprensión de archivos en formato 7z, que se pueden comprimir y enviar cifrados.



💻 Aplicación offline instalable en GNU/Linux, DOS, Mac OS, Windows, 

Licencia: AGPL, BSD

Alternativa a: WinZip, WinRAR



🔗 Página del programa: [https://www.7-zip.org/](https://www.7-zip.org/)

🔗 Repositorio:  [https://github.com/kornelski/7z](https://github.com/kornelski/7z)

🔗 Foro de ayuda: [https://www.7-zip.org/faq.html](https://www.7-zip.org/faq.html)



🗺️ Aragonés, Asturianu, Castellano/Español, Català/Valencià, English, Euskera, Galego







## **4. UTILIDADES Y OTRAS TAREAS DE INVESTIGACIÓN**

### **Gestor bibliográfico**

#### Zotero

Gestor bibliográgico que recopila informació de los artículos automáticamente, permite la citación de referencias y se sincroniza con su versión web. 



💻🌐 Aplicación instalable en GNU/Linux, Mac Os, Windows y web (no necesita ser instalada)

Licencia: AGPL

Alternativa a: Mendeley, EndNote

💸  Hasta 300 megabytes es gratuito, una capacidad de almacenamiento mayor tiene un precio mínimo de 20 dólares al año ([https://www.zotero.org/storage)](https://www.zotero.org/storage))



🔗 Página del programa: [https://www.zotero.org/](https://www.zotero.org/)

🔗 Repositorio:  [https://github.com/zotero](https://github.com/zotero)

🔗 Foro de ayuda: [https://www.zotero.org/support/](https://www.zotero.org/support/) / [https://forums.zotero.org/discussions](https://forums.zotero.org/discussions)



🗺️  English





### **Gestión editorial**

#### Open Journal Systems
Proyecto de Public Knowledge Project para la gestión y publicación de revistas académicas de acceso abierto.



💻 Aplicación instalable en FreeBSD, GNU/Linux, Mac Os, Solaris, Windows.

Licencia: AGPL

🔗 Página del programa: [https://pkp.sfu.ca/](https://pkp.sfu.ca/)

🔗 Repositorio: [https://github.com/pkp/ojs](https://github.com/pkp/ojs)


🗺️  Castellano/Español, English



#### Polar

Gestor bibliográgico para contenido web y documentos, que permite el etiquetado, la anotación, el subrayado y el seguimiento del progreso de lectura. 



💻🌐 Aplicación offline instalable en GNU/Linux, Mac OS X, Windows y aplicación web (no necesita ser instalada)

Licencia: GPL

Alternativa a: Mendeley, EndNote

💸 Existe una versión gratiota de 350 megabytes y el plan inmediatamente superior tiene un coste de 4,99 dólares al mes ([https://getpolarized.io/#pricing)](https://getpolarized.io/#pricing))



🔗 Página del programa: [https://getpolarized.io/](https://getpolarized.io/)

🔗 Repositorio: [https://github.com/burtonator/polar-bookshelf](https://github.com/burtonator/polar-bookshelf)

🔗 Foro de ayuda: [https://getpolarized.io/docs/](https://getpolarized.io/docs/)



🗺️ English





### **Editor de CV**

#### Europass

Aplicación para la creación de un currículum vitae normalizado a nivel comunitario (Unión Europea).



🌐 Aplicación web (no necesita ser instalada)

Licencia: MIT

Alternativa a: Zety, VisualCV

💸  Proyecto público desarrollado por DECEFOP (European Centre for the Development of Vocational Training)



🔗 Página del programa: [http://europass.cedefop.europa.eu](http://europass.cedefop.europa.eu)

🔗 Repositorio:  [https://github.com/europass](https://github.com/europass)

🔗 Foro de ayuda: [https://www.cedefop.europa.eu/en/about-cedefop/faqs](https://www.cedefop.europa.eu/en/about-cedefop/faqs)



🗺️ Castellano/Español, English





### **Navegador**

#### Tor (The Onion Router)

Navegador centrado en la privacidad, borra automáticamente las cookies y el historial de visita al finalizar la sesión. La información viaja a través de nodos (o routers cebolla) entre el origen y el destino que impiden el seguimiento de terceros. 



💻📱 Aplicación instalable en: GNU/Linux, BSD, Mac OS, Windows, Android e iOS

Licencia: BSD

Alternativa a: Google Chrome, Microsoft Edge

💸 Se puede realizar donaciones voluntarias. The Tor Project propone un único ingreso de 125 dólares ([https://donate.torproject.org/es\_ES)](https://donate.torproject.org/es\_ES))



🔗 Página del programa: [https://www.torproject.org/](https://www.torproject.org/)

🔗 Repositorio: [https://github.com/TheTorProject](https://github.com/TheTorProject)

🔗 Foro de ayuda: [https://tb-manual.torproject.org/](https://tb-manual.torproject.org/) / [https://support.torproject.org/](https://support.torproject.org/)



🗺️ Castellano/Español, Català/Valencià, English





#### Mozilla Firefox

Navegador multiplataforma que es posible complementar con el uso de Firefox Send (mencionado en este manual), Firefox Lockwise (gestión de contraseñas) y Firefox Monitor (para conocer si la información personal ha sido filtrada).



💻📱 Aplicación instalable en: GNU/Linux, Windows, Mac Os, Android e iOS

Licencia: MPL

Alternativa a: Google Chrome, Microsoft Edge

💸 Existe una página de donación al proyecto Mozilla, donde proponen una donación mínima de 15 euros ([https://donate.mozilla.org/es/)](https://donate.mozilla.org/es/))



🔗 Página del programa: [https://www.mozilla.org/es-ES/firefox/new/](https://www.mozilla.org/es-ES/firefox/new/)

🔗 Repositorio: [https://github.com/mozilla](https://github.com/mozilla)

🔗 Foro de ayuda: [https://support.mozilla.org/es/products?utm\_source=mozilla.org\&utm\_medium=referral\&utm\_campaign=need-help-link](https://support.mozilla.org/es/products?utm\_source=mozilla.org\&utm\_medium=referral\&utm\_campaign=need-help-link)



🗺️ Aragonés, Asturianu, Castellano/Español, Català/Valencià, Galego, English





#### DuckDuckGo

Motor de búsqueda con características especiales para el pronóstico del tiempo, la hora, el tiempo, al conversión de moneda, la calculadora o los mapas. 



🌐 Aplicación web (no necesita ser instalada)

Licencia: Apache

Alternativa a: Google



🔗 Página del programa: [https://duckduckgo.com/](https://duckduckgo.com/)

🔗 Repositorio: [https://github.com/duckduckgo](https://github.com/duckduckgo)

🔗 Foro de ayuda: [https://help.duckduckgo.com/](https://help.duckduckgo.com/)



🗺️  Castellano/Español, Català/Valencià, Galego, English





### **Antivirus**

#### Clam AntiVirus

Software antivirus especializado en el escaneo de correo electrónico. También te sirve para archivos comprimidos, macros de Microsoft Office y análisis del sistema. 



💻 Aplicación offline instalable en: GNU/Linux, BSD, Mac OS y Windows

Licencia: GPL

Alternativa a: Avast Free Antivirus, Windows Defender, ESET NOD32 Antivirus, AVG AntiVirus



🔗 Página del programa: [https://www.clamav.net](https://www.clamav.net)

🔗 Repositorio: [https://github.com/Cisco-Talos/clamav-faq](https://github.com/Cisco-Talos/clamav-faq)

🔗 Foro de ayuda: [https://www.clamav.net/documents/clam-antivirus-user-manual](https://www.clamav.net/documents/clam-antivirus-user-manual)



🗺️ English







## Oficinas de software libre en la universidad 

A continuación se ofrece un listado de las oficinas de software libre pertenecientes a las universidades españolas. Se recomienda visitarlas, pues además de la oferta de software específico -que responde a las necesidades de la comunidad investigadora y docente-, algunas de ellas también hacen cursos y tutoriales para facilitar su uso y adaptación.



#### **Universitat Autònoma de Barcelona**

Oficina de Programari Lliure de la Universitat Autònoma de Barcelona

🔗 [https://opl.uab.es/](https://opl.uab.es/)

Catálogo de progamari lliure

🔗 [http://www.ub.edu/informaticapersonal/programarilliure/#open](http://www.ub.edu/informaticapersonal/programarilliure/#open)



#### **Universidad Carlos III de Madrid**

Oficina de Software Libre de la Universidad Carlos III de Madrid

🔗[https://osl.uc3m.es/](https://osl.uc3m.es/)



#### **Universidad Complutense de Madrid**

Librelab de la Universidad Complutense de Madrid

🔗 [https://librelabucm.org/](https://librelabucm.org/)



#### **Universitat d'Alacant**

Coneixement Obert i Programari Lliure a la Universitat d'Alacant

🔗 [https://blogs.ua.es/copla/](https://blogs.ua.es/copla/)



#### **Univesidade da Coruña**

Oficina de Software Libre do CIXUG

🔗 [https://www.cixug.es/](https://www.cixug.es/)



#### **Universidad de Almería**

Oficina de Software Libre de la Asociación de Universitarios Informáticos para la Universidad de Almería

🔗 [http://asociacion-unia.es/](http://asociacion-unia.es/)



#### **Universidad de Burgos**

Catálogo de alternativas de software libre

🔗 [https://www.ubu.es/servicio-de-informatica-y-comunicaciones/catalogo-de-servicios/software-tu-disposicion/alternativas-de-software-libre](https://www.ubu.es/servicio-de-informatica-y-comunicaciones/catalogo-de-servicios/software-tu-disposicion/alternativas-de-software-libre)

#### 

#### **Universidad de Córdoba**

Aula de Software Libre de la Universidad de Córdoba

🔗 [https://www.uco.es/aulasoftwarelibre/](https://www.uco.es/aulasoftwarelibre/)



#### **Universidad de Cádiz**

Oficina del Software Libre y Conocimiento Abierto de la Universidad de Cádiz

🔗 [https://cau-personal.uca.es/cau/grupoServicios.do?id=SWL](https://cau-personal.uca.es/cau/grupoServicios.do?id=SWL)



#### **Universidad de Deusto**

ESIDE’s GNU Hi-tech and Open Source Team de la Universidad de Deusto

🔗 [http://softwarelibre.deusto.es/](http://softwarelibre.deusto.es/)



#### **Universidad de Huelva**

Portal de Software Libre de la Universidad de Huelva

🔗 [http://petrel.uhu.es/](http://petrel.uhu.es/)



#### **Universidad de Granada**

Oficina de Software Libre de la Universidad de Granada

🔗 [https://osl.ugr.es/](https://osl.ugr.es/)

Catálogo de software libre en el Centro de Servicios Informáticos y Redes de Comunicación

🔗 [https://csirc.ugr.es/informatica/ServiciosCorporativos/SoftwareLibre/](https://csirc.ugr.es/informatica/ServiciosCorporativos/SoftwareLibre/)



#### **Universidad de La Laguna**

Oficina de Software Libre de la Universidad de La Laguna

🔗 [https://www.ull.es/servicios/osl/](https://www.ull.es/servicios/osl/)



#### **Universidad de Las Palmas de Gran Canaria**

Oficina de Software Libre de la Universidad de Las Palmas de Gran Canaria

🔗 [https://osl.ulpgc.es/](https://osl.ulpgc.es/)



#### **Universidad de Murcia**

Software Libre y Abierto en la Universidad de Murcia

🔗 [https://www.um.es/web/softla/](https://www.um.es/web/softla/)



#### Universidad de Oviedo

 Recopilación de software libre útil para su trabajo en la Biblioteca

 🔗 [https://buo.uniovi.es/servicios/herramientas/softwarelibre](https://buo.uniovi.es/servicios/herramientas/softwarelibre)



#### **Universidad de Salamanca**

Software libre disponible para la instalación en ordenadores de despacho y casa

🔗 [https://sicpd.usal.es/otros-recursos/software-disponible-para-la-comunidad-universitaria](https://sicpd.usal.es/otros-recursos/software-disponible-para-la-comunidad-universitaria)



#### **Universidad de Sevilla**

Grupo de trabajo «Software Libre – Fuente Abierta» de la Universidad de Sevilla

🔗 [https://solfa.us.es/](https://solfa.us.es/)



#### **Universidad de Zaragoza**

Oficina de Software Libre de la Universidad de Zaragoza

🔗 [https://osluz.unizar.es/](https://osluz.unizar.es/)



#### **Universidad Miguel Hernández**

Oficina de Software y Hardware Libre de la Universidad Miguel Hernández

🔗 [http://oshl.edu.umh.es/](http://oshl.edu.umh.es/)



#### **Universidade de Santiago de Compostela**

Oficina de Software Libre do CIXUG

🔗 [https://www.cixug.es/](https://www.cixug.es/)



#### **Universidade de Vigo**

Oficina de Software Libre do CIXUG

🔗 [https://www.cixug.es/](https://www.cixug.es/)



Finalmente, ante una necesidad específica y una respuesta más rápida, aconsejamos visitar la web  ALTERNATIVE TO ([https://alternativeto.net/)](https://alternativeto.net/)), que ofrece alternativas al software específico que conocemos. Las alternativas que ofrece son variadas, no se limitan a software libre, pero explicitan el tipo de licencia que tienen siempre.


Autoras (por orden alfabético): Dafne Calvo y Lorena Cano-Orón

**Esta obra se encuentra bajo Creative Commons Reconocimiento 4.0 Internacional**

🔗 [https://creativecommons.org/licenses/by/4.0/legalcode.es](https://creativecommons.org/licenses/by/4.0/legalcode.es)
